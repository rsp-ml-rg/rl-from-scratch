import tensorflow as tf
from configuration import ConfigurableObject


class ModelBase(ConfigurableObject):

    def __init__(self, n_actions, long_name='model', short_name='m'):
        super().__init__(long_name, short_name)
        self._n_actions = n_actions

    def select_action(self, obs):
        """
        Selects an action based on the current observation (and possibly internal memory)
        :param obs: current observation
        :return: action
        """
        raise NotImplementedError
        return action


class ActorModelBase(ModelBase):
    '''
    Abstract class for discrete-action RL models
    '''

    def __init__(self, n_actions):
        super().__init__(n_actions=n_actions)

    def action_logits(self, obs_ph):
        raise NotImplemented("Need ot implement action selection!")
        return action_logits

    def select_action(self, obs):
        action_logits = self.action_logits(obs)
        # Original:
        # selected_action = tf.random.multinomial(action_logits, 1)
        # From OAI implementation:
        selected_action = tf.squeeze(tf.multinomial(logits=action_logits, num_samples=1), axis=1)
        return selected_action, action_logits
