import tensorflow as tf
from configuration import ConfigurableObject
from models.model_base import ModelBase


class QFunction(ConfigurableObject):

    def __init__(self, n_actions, parent, function='mlp', layers=(5, 5), long_name='q_function', short_name='qf'):

        super().__init__(long_name, short_name, parent)
        self.add_parameter('function', 'f', type=str, default=function)
        self.add_parameter('layers', 'l', type=list, default=layers)
        self.n_actions = n_actions

        self.function_dict = {
            'mlp': self.mlp_q_function
        }

        self._mlp_layers = [tf.layers.Dense(l) for l in layers] + [tf.layers.Dense(n_actions)]

    def mlp_q_function(self, obs):
        '''
        MLP q value estimator for a fixed discrete set of actions. Last layer outputs a vector of q-values for the actions.
        :param obs: observation features
        :param n_actions: int: number of actions
        :param layers:
        :return:
        '''
        x = obs
        for l in self._mlp_layers:
            x = l(x)

        return x

    def __call__(self, obs):
        function = self.function_dict[self['function']]
        return function(obs)


class EGreedy(ModelBase):
    '''
    Abstract class for discrete-action RL models
    '''

    def __init__(self, n_actions, q_function='mlp', layers=(5, 5)):
        '''

        :param n_actions: number of actions. Expects the q_function to output a vector of this length.
        :param epsilon: Exploration rate.
        :param q_function: Q-function expected to take an n_actions key-word argument
        '''
        super().__init__(n_actions)

        self.add_parameter('epsilon', 'e', type=float, default=0.1)
        self.n_actions = n_actions
        self.q_function = QFunction(n_actions=n_actions, parent=self, function=q_function, layers=layers)

    def select_action(self, obs):
        q_vals = self.q_function(obs)
        epsilon = tf.constant(self['epsilon'], dtype=tf.float32)
        probs = tf.one_hot(tf.arg_max(q_vals, dimension=1), depth=self.n_actions) * (1. - epsilon) + \
                tf.ones((1, q_vals.shape[1])) * (epsilon / tf.cast(tf.shape(q_vals)[1], tf.float32))

        selected_action = tf.squeeze(tf.multinomial(logits=tf.log(probs), num_samples=1), axis=1)
        return selected_action, q_vals
