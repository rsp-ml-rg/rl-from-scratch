import tensorflow as tf

from models.model_base import ActorModelBase


class SimpleMLPActorModelBase(ActorModelBase):

    def __init__(self, n_actions, layers=(16, 8, 8)):

        super().__init__(n_actions)
        self.add_parameter('layers', 'l', type=list, default=layers)

    def action_logits(self, obs):
        x = obs
        for l in self['layers']:
            x = tf.layers.Dense(l)(x)
        return tf.layers.Dense(self._n_actions)(x)
