from gym.envs.classic_control.cartpole import CartPoleEnv
from configuration import configuration
from models.simple_mlp_model import SimpleMLPActorModelBase
from trainers.vpg_trained_baseline import VPGTrainedBaseline


env = CartPoleEnv()
env._configuration = {}

config = {
    'trainer.learning_rate': 0.001,
    'trainer.baseline_learning_rate': 0.01,
    'trainer.batch_size': 256
}

configuration.add_config(config)

model = SimpleMLPActorModelBase(n_actions=env.action_space.n)

trainer = VPGTrainedBaseline(model, env, epochs=1000)
trainer.run_training()
