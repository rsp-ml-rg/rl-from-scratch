from gym.envs.classic_control.cartpole import CartPoleEnv
from configuration import configuration
from models.e_greedy import EGreedy
from trainers.td_trainer import TDTrainer


env = CartPoleEnv()
env._configuration = {}

n_actions = env.action_space.n
print(f"Number of actions: {n_actions}")


config = {
    'trainer.td_steps': 5,
    'model.epsilon': 0.05,
    'model.q_function.layers': (5,5),
    'trainer.episodes': int(1e6),
    'trainer.learning_rate': 0.001,
    'trainer.replay_memory_renewal': 'random',
    'trainer.replay_memory_size': 50000
}
configuration.add_config(config)

model = EGreedy(n_actions=n_actions)

trainer = TDTrainer(model, env)
trainer.run_training()
