from gym.envs.classic_control.cartpole import CartPoleEnv
from configuration import configuration
from models.e_greedy import EGreedy
from trainers.td_trainer import TDTrainer

n_experiments = 100

env = CartPoleEnv()
env._configuration = {}

n_actions = env.action_space.n
print(f"Number of actions: {n_actions}")


common_config = {
    'trainer.steps': int(1e7),
}

param_sampling_sets = {
    'trainer.td_steps': [1,2,3,4,5,7,9,12,15,18,25],
    'model.epsilon': [0.01, 0.05, 0.1, 0.2],
    'model.q_function.layers': [(3,3), (5,5), (10,5), (5,10), (10,10), (5,5,5), (10, 5, 5), (10,5,10), (20,20)],
    'trainer.learning_rate': [0.1, 0.05, 0.02, 0.01, 0.005, 0.002, 0.001, 0.0005],
    'trainer.replay_memory_renewal': ['sequential', 'random'],
    'trainer.replay_memory_size': [5000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000]
}

for n in range(n_experiments):

    configuration.reset()
    configuration.add_config(common_config)

    model = EGreedy(n_actions=n_actions)

    trainer = TDTrainer(model, env)

    configuration.randomly_sample(param_sampling_sets)

    trainer.run_training()
