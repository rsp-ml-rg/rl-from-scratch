from gym.envs.classic_control.cartpole import CartPoleEnv

from models.simple_mlp_model import SimpleMLPActorModelBase
from trainers.vpg import VPGTrainer


env=CartPoleEnv()
env._configuration = {}

model = SimpleMLPActorModelBase(n_actions=env.action_space.n)

trainer = VPGTrainer(model, env, epochs=200)
trainer.run_training()
