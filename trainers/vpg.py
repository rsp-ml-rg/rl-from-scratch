import tensorflow as tf
import numpy as np
from trainers.trainer_base import TrainerBase
from utils import flatten_shape


class VPGTrainer(TrainerBase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._state.add_variable('epoch_observations_tf', [], epoch_start_action=lambda _: [])
        self._state.add_variable('epoch_actions_t', [], epoch_start_action=lambda _: [])
        self._state.add_variable('episode_rewards_t', [], episode_start_action=lambda _: [])
        self._state.add_variable('epoch_returns_t', [], epoch_start_action=lambda _: [])
        self._state.add_variable('loss', 0, log=True)


    def build_graph(self):

        self._graph_inputs['observation_bf'] = tf.placeholder(tf.float32,
                                                              shape=(None, flatten_shape(self._env.observation_space.shape)),
                                                              name='observation_ph')
        self._graph_inputs['advantage_b'] = tf.placeholder(tf.float32, shape=(None, ), name='advantage_ph')
        self._graph_inputs['action_t'] = tf.placeholder(tf.int32, shape=(None, ), name='action_ph')

        obs_bf = self._graph_inputs['observation_bf']
        self._model_outputs['action'], self._model_outputs['action_logits_ba'] = self._model.select_action(obs_bf)

        action_mask_ta = tf.one_hot(self._graph_inputs['action_t'], self._env.action_space.n)
        action_log_probs = tf.reduce_sum(action_mask_ta*tf.nn.log_softmax(self._model_outputs['action_logits_ba']),
                                         axis=1)

        self._loss = - tf.reduce_mean(action_log_probs * self._graph_inputs['advantage_b'])
        self._optimizer = self._optimizer(self['learning_rate'])
        self._train_op = self._optimizer.minimize(self._loss)

    def step_update(self):
        self._state['epoch_observations_tf'].append(self._state['observation'])
        self._state['epoch_actions_t'].append(self._state['action'])
        self._state['episode_rewards_t'].append(self._state['reward'])

    def episode_update(self):
        episode_returns_t = np.cumsum(self._state['episode_rewards_t'])[::-1] # TODO: This was written for constant reward per step. Does not generalize!!! Rewrite!
        self._state['epoch_returns_t'].extend(episode_returns_t)

    def epoch_update(self):

        self._state['loss'], _ = self._sess_run((self._loss, self._train_op), feed_dict={'observation_bf': self._state['epoch_observations_tf'],
                                                  'action_t': self._state['epoch_actions_t'],
                                                  'advantage_b': self._state['epoch_returns_t']})

