import tensorflow as tf
import numpy as np
from werd import WerdConnection
from werd_config import werd_config
from configuration import configuration, ConfigurableObject
from utils import get_revision_hash, flatten_shape
import time
import os
import re

class TrainerState(object):

    def __init__(self):
        self._variables = {}
        self._secondary_variables = {}

    def check_key_free(self, key):
        if key in self._variables or key in self._secondary_variables:
            raise ValueError(f"Attempting to add the key {key} for a second time to the TrainerState")

    def add_variable(self, name, initial_value=None, episode_start_action=None, episode_end_action=None,
                     update_end_action=None, log=False, override=False):
        '''
        Register a new variable in the state.
        :param name: str: key under which the variable will be retrievable from the state
        :param initial_value:
        :param episode_start_action: function called at the beginning of each epoch with 1 argument (the previous value
                                     of this variable) to set a new value for the variable. It can be used e.g. to
                                     reset the variable to 0 by setting epoch_start_action=lambda v: 0
        :param episode_end_action: see episode_start_action
        :param update_end_action: see action invoked after each training update
        :param log: bool: should this variable be recorded in the logs
        :param override: bool: allow adding this variable even if one is already registered under the same name
        :return: None
        '''

        if not override:
            self.check_key_free(name)
        self._variables[name] = {
            'value': initial_value,
            'episode_start_action': episode_start_action,
            'episode_end_action': episode_end_action,
            'update_end_action': update_end_action,

            'log': log
        }

    def add_secondary_variable(self, name, value_factory=lambda state, internal_state: internal_state,
                               update_end_action=None, episode_start_action=None, episode_end_action=None,
                               initial_internal_value=None, log=True):
        '''
        Similar to add_variable. However, a secondary variable can depend on other variables. Hence when called
        it uses value factory - this takes the whole state and an internal value of this variable as arguments
        and returns a value. Similarly, the epoch/episode start/end actions take state, internal_value and set
        a new internal value.
        :param name:
        :param value_factory:
        :param update_end_action:
        :param episode_start_action:
        :param episode_end_action:
        :param initial_internal_value:
        :param log:
        :return:
        '''

        self.check_key_free(name)
        self._secondary_variables[name] = {
            'factory': value_factory,
            'log': log,
            'update_end_action': update_end_action,
            'episode_start_action': episode_start_action,
            'episode_end_action': episode_end_action,
            'internal_value': initial_internal_value
        }

    def apply_action(self, action_type):
        for var in self._variables.values():
            if var[action_type] is not None:
                var['value'] = var[action_type](var['value'])

        for var in self._secondary_variables.values():
            if var[action_type] is not None:
                var['internal_value'] = var[action_type](self, var['internal_value'])

    def episode_start(self):
        self.apply_action('episode_start_action')

    def episode_end(self):
        self.apply_action('episode_end_action')

    def update_end(self):
        self.apply_action('update_end_action')

    def vars_to_log(self):
        d = {k: var['value'] for k, var in self._variables.items() if var['log']}
        d.update({k: self[k] for k, var in self._secondary_variables.items() if var['log']})
        return d

    def __getitem__(self, key):
        if key in self._variables:
            return self._variables[key]['value']
        else:
            var = self._secondary_variables[key]
            try:
                return var['factory'](self, var['internal_value'])
            except ZeroDivisionError:
                return np.nan

    def __setitem__(self, key, value):
        self._variables[key]['value'] = value

    def set_internal_value(self, k, v):
        self._secondary_variables[k]['internal_value'] = v


class TrainerBase(ConfigurableObject):

    def __init__(self, model, env,
                 optimizer=tf.train.AdamOptimizer,
                 tensorboard_log_dir=os.path.join(werd_config['local_save_dir'],'tensorboard'),
                 long_name='trainer', short_name='t'):
        """
        WARNING: Most of these parameters are just defaults and can be overriden using command line arguments or
                 config dict
        :param model:
        :param env:
        :param optimizer: a tf.train.Optimizer subclass (not instantiated)

        """
        super().__init__(long_name, short_name)

        self.add_parameter('episodes', 'e', type=int, default=int(1e6),
                           description='Terminate training after e episodes.')
        self.add_parameter('steps', 's', type=int, default=None,
                           description='Terminate training after a total of s environment steps.')
        self.add_parameter('finish_final_episode', 'ffe', type=bool, default=True,
                           description='When max amount of steps is reached, finish the current episode '
                                       'before terminating training.')

        self.add_parameter('update_every', 'ue', type=str, default='1bs',
                           description='How often to trigger training update. Integer (optionally followed by "b" to'
                                       'multiply it by batch size) followed by unit (s for steps, e for episodes)')
        self.add_parameter('update_after_episode', 'uae', type=bool, default=True)

        self.add_parameter('max_steps_per_episode', 'spe', type=int, default=1000)
        self.add_parameter('batch_size', 'b', type=int, default=1024)
        self.add_parameter('learning_rate', 'lr', type=float, default=0.01)


        self._model = model
        self._env = env
        self.obs_dim = flatten_shape(self._env.observation_space.shape)

        # To be instantiated within the build_graph method
        self._optimizer = optimizer

        self._session = None

        # handles on key parts of the computation graph
        self._loss = None
        self._train_op = None

        self._model_outputs = {}

        self._graph_inputs = {}
        self._graph_outputs = {}

        self._state = TrainerState()
        # Variables to be handled manually:
        self._state.add_variable('reward')
        self._state.add_variable('observation')
        self._state.add_variable('last_observation')
        self._state.add_variable('action')
        self._state.add_variable('steps')
        self._state.add_variable('episode_return', episode_start_action=lambda _: 0, log=False)
        self._state.add_variable('total_reward', 0, log=True)
        self._state.add_variable('total_steps', initial_value=0, log=True)
        self._state.add_variable('steps_since_update', initial_value=0, log=True, update_end_action=lambda _: 0)
        self._state.add_variable('reward_since_update', initial_value=0, log=False, update_end_action=lambda _: 0)

        # Automatically tracked variables:
        self._state.add_variable('episode', initial_value=0, log=True,
                                 episode_start_action=lambda e: e+1)
        self._state.add_variable('episodes_since_update', initial_value=0, log=True,
                                 episode_start_action=lambda e: e+1, update_end_action=lambda _: 0)
        self._state.add_variable('updates', initial_value=0, update_end_action=lambda v: v+1, log=True)

        self._state.add_secondary_variable('avg_steps_per_episode', log=True,
                                           value_factory=lambda state, internal_state: state['steps_since_update']/state['episodes_since_update'])
        self._state.add_secondary_variable('avg_return_per_episode', log=True,
                                           value_factory=lambda state, internal_state: state['reward_since_update'] / state['episodes_since_update'])

        self._tb_log_dir = tensorboard_log_dir

        # To be initialized in self.initialize()
        self._experiment_id = None
        self._werd = None
        self._tb_summary_writer = None
        self._update_every = (None, None)

    def _initialize_werd(self):
        # Connection to Ondrej's result database
        self._werd = WerdConnection(**werd_config)

        repo = 'gitlab.com/rsp-ml-rg/rl-from-scratch'
        revision_hash = get_revision_hash()

        self._werd.update({'_id': self._experiment_id,
                           'metadata': {
                                'repository': repo,
                                'revision_hash': revision_hash
                            },
                           'configuration':
                                configuration.param_dict
                           })

    def experiment_id(self):
        trainer_name = self.__class__.__name__
        model_name = self._model.__class__.__name__
        env_name = self._env.__class__.__name__

        timestamp = time.strftime('%y%m%d_%H%M')

        _id = '_'.join([trainer_name, model_name, env_name, configuration.param_id_string(), timestamp])

        return _id

    def parse_update_frequency(self):
        m = re.match(r'(\d+)(b?)([es])$', self['update_every'])
        if not m:
            raise ValueError("The trainer.update_every parameter must match \d+b?[es]")
        update_freq = int(m.group(1))
        if m.group(2) == 'b':
            update_freq *= self['batch_size']

        self._update_every = (update_freq, m.group(3))

    def initialize(self):
        self.parse_update_frequency()

        self._session = tf.Session()
        init = tf.global_variables_initializer()

        self._session.run(init)

        self._experiment_id = self.experiment_id()
        self._initialize_werd()
        self._tb_summary_writer = tf.summary.FileWriter(logdir=os.path.join(self._tb_log_dir, self._experiment_id),  # session=self._session,
                                                        graph=self._session.graph, filename_suffix='.'+self._experiment_id)
        print(f"Experiment id: {self._experiment_id}")

    def build_graph(self):
        raise NotImplementedError("Need to implement build_graph.")

    def step_update(self):
        pass

    def episode_update(self):
        pass

    def training_update(self):
        pass

    def update_counters(self, step, reward):
        state = self._state

        state['steps'] = step
        state['total_steps'] += 1
        state['steps_since_update'] += 1

        state['episode_return'] += reward
        state['reward_since_update'] += reward
        state['total_reward'] += reward


    def run_training(self):
        configuration.parse_arguments()
        self.build_graph()
        self.initialize()

        state = self._state

        for episode in range(self['episodes']):

            state['observation'] = self._env.reset()
            state['last_observation'] = state['observation']
            state.episode_start()

            for s in range(self['max_steps_per_episode']):

                action_1f = self._sess_run(self._model_outputs['action'],
                                           feed_dict={'observation_bf': [state['observation']]})
                state['action'] = np.squeeze(action_1f)
                state['observation'], state['reward'], done, _ = self._env.step(state['action'])

                self.update_counters(s, state['reward'])

                self.step_update()

                if self._update_every[1] == 's' and not self['update_after_episode'] and \
                        state['steps_since_update'] >= self._update_every[0]:
                    self.training_update()
                    self.log_write(self._state.vars_to_log())
                    state.update_end()

                state['last_observation'] = state['observation']

                if done or (not self['finish_final_episode'] and state['total_steps'] >= self['steps']):
                    break

            if not self['finish_final_episode'] and state['total_steps'] >= self['steps']:
                break

            self.episode_update()
            state.episode_end()

            if (self._update_every[1] == 's' and state['steps_since_update'] >= self._update_every[0]) or \
               (self._update_every[1] == 'e' and state['episodes_since_update'] >= self._update_every[0]):
                self.training_update()
                self.log_write(self._state.vars_to_log())
                state.update_end()

            if state['total_steps'] >= self['steps']:
                break

        self.log_write(self._state.vars_to_log())
        self._werd.submit_results()

    def dict_to_tensorboard(self, value_dict):
        '''
        Records values from a dict to Tensorboard
        :param value_dict: flat dict of key-value pairs to log. Should contain 'epoch'.
        :return: None
        '''
        values = [tf.Summary.Value(tag=k, simple_value=v) for k, v in value_dict.items()]
        summary = tf.Summary(value=values)
        event = tf.summary.Event(summary=summary,
                                 wall_time=time.time(),
                                 step=value_dict['total_steps'])

        self._tb_summary_writer.add_event(event=event)
        self._tb_summary_writer.flush()

    def log_write(self, value_dict):
        '''
        Records the value_dict into werd, Tensorboard, and STD_OUT
        :param value_dict: expecting a flat dict (because of tensorboard) TODO: handle non-flat cases
        :return: None
        '''
        print(value_dict)
        self._werd.append_result(value_dict)
        self.dict_to_tensorboard(value_dict)

    def _sess_run(self, *args, feed_dict={}, **kwargs):

        def convert_key(k):
            '''
            Converts string keys to corresponding placeholders from self._graph_inputs
            :param k: str or tf.placeholder
            :return: tf.placeholder
            '''
            if isinstance(k, str):
                return self._graph_inputs[k]
            else:
                return k

        new_feed_dict = {convert_key(k): v for k, v in feed_dict.items()}

        return self._session.run(*args, feed_dict=new_feed_dict, **kwargs)