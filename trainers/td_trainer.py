import tensorflow as tf
import numpy as np
from trainers.trainer_base import TrainerBase
import random


class TDTrainer(TrainerBase):

    def __init__(self, *args, gamma=0.99, replay_memory_size=10000, replay_memory_renewal='sequential', td_steps=1, **kwargs):

        super().__init__(*args, **kwargs)

        self._gamma = gamma

        self.add_parameter('replay_memory_size', 'rms', type=int, default=replay_memory_size)
        self.add_parameter('replay_memory_renewal', 'rmr', type=str, default=replay_memory_renewal)
        self.add_parameter('td_steps', 'tds', type=int, default=td_steps)
        self.add_parameter('gamma', 'g', type=float, default=gamma)
        self.add_parameter('zeros_at_episode_end', 'zee', type=bool, default=True)

        self._state.add_variable('observation_memory', episode_start_action=lambda _: [])
        self._state.add_variable('action_memory', episode_start_action=lambda _: [])
        self._state.add_variable('reward_memory', episode_start_action=lambda _: [])
        self._state.add_variable('loss', log=True)

        self._replay_memory = []
        self._discount_vector = None

    def initialize(self):
        super().initialize()

        self._replay_memory = []
        self._discount_vector = np.array([self['gamma']**k for k in range(self['td_steps'])])

    def build_graph(self):

        self._graph_inputs['observation_bf'] = tf.placeholder(tf.float32, shape=(None, self.obs_dim), name='observation_ph')
        self._graph_inputs['return_target_b'] = tf.placeholder(tf.float32, shape=(None,), name='advantage_ph')
        self._graph_inputs['action_b'] = tf.placeholder(tf.int32, shape=(None, ), name='action_ph')

        # Action selection
        action_b, _ = self._model.select_action(self._graph_inputs['observation_bf'])
        self._model_outputs['action'] = action_b

        # Calculation of q_values:
        action_t = self._graph_inputs['action_b']
        q_values_ba = self._model.q_function(self._graph_inputs['observation_bf'])
        self._graph_outputs['max_q_val_b'] = tf.reduce_max(q_values_ba, axis=-1)
        action_mask_ta = tf.one_hot(action_t, self._env.action_space.n)
        selected_action_value_b = tf.reduce_sum(action_mask_ta * q_values_ba, axis=1)

        self._loss = tf.losses.mean_squared_error(self._graph_inputs['return_target_b'], selected_action_value_b)
        tf.summary.scalar('loss', self._loss)
        self._optimizer = self._optimizer(self['learning_rate']) # TODO: ugly - rewrite
        self._train_op = self._optimizer.minimize(self._loss)

    def memorize_step(self, obs1, action, n_step_return, obs2, bootstrap):
        if len(self._replay_memory) >= self['replay_memory_size']:
            if self['replay_memory_renewal'] == 'sequential':
                self._replay_memory.pop(0)
            elif self['replay_memory_renewal'] == 'random':
                self._replay_memory.pop(np.random.randint(len(self._replay_memory)))
            else:
                raise ValueError("Unknown replay_memory_renewal method.")

        self._replay_memory.append((obs1, action, n_step_return, obs2, bootstrap))

    def step_update(self):

        self._state['observation_memory'].append(self._state['last_observation'])
        self._state['reward_memory'].append(self._state['reward'])
        self._state['action_memory'].append(self._state['action'])

        # If we have done enough steps, make a record in replay memory
        if len(self._state['observation_memory']) == self['td_steps']:
            n_step_return = np.sum(self._discount_vector * np.array(self._state['reward_memory']))
            self._state['reward_memory'].pop(0)
            orig_observation = self._state['observation_memory'].pop(0)
            orig_action = self._state['action_memory'].pop(0)

            bootstrap_observation = self._state['observation']

            self.memorize_step(orig_observation, orig_action, n_step_return, bootstrap_observation, 1)

    def episode_update(self):

        mem_len = len(self._state['reward_memory'])

        if mem_len < self['td_steps']-1:
            self._state['reward_memory'].extend([0]*(self['td_steps']-1-mem_len))

        if self['zeros_at_episode_end']:
            for _ in range(self['td_steps']-1):
                self._state['reward_memory'].append(0)
                n_step_return = np.sum(self._discount_vector * self._state['reward_memory'])
                self._state['reward_memory'].pop(0)
                try:
                    orig_observation = self._state['observation_memory'].pop(0)
                except IndexError:
                    break
                orig_action = self._state['action_memory'].pop(0)
                bootstrap_observation = [1] * self.obs_dim

                self.memorize_step(orig_observation, orig_action, n_step_return, bootstrap_observation, 0)

    def training_update(self):
        # Prepare replay step from memory
        batch_data_b = random.choices(self._replay_memory, k=self['batch_size'])
        obs_bf, action_b, n_step_return, bootstrap_obs_bf, bootstrap_coeff_b = zip(*batch_data_b)

        predicted_future_val_b = self._session.run(self._graph_outputs['max_q_val_b'],
                                                   feed_dict={self._graph_inputs['observation_bf']: bootstrap_obs_bf})
        q_value_target_b = n_step_return + (self._gamma**self['td_steps']) * predicted_future_val_b * bootstrap_coeff_b

        loss, _ = self._session.run((self._loss, self._train_op),
                                    feed_dict={self._graph_inputs['observation_bf']: obs_bf,
                                               self._graph_inputs['action_b']: action_b,
                                               self._graph_inputs['return_target_b']: q_value_target_b})
        self._state['loss'] = loss

