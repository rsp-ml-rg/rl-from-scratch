from trainers.vpg import VPGTrainer
from configuration import ConfigurableObject
import tensorflow as tf
import numpy as np


class MLPValueModel(ConfigurableObject):

    def __init__(self, layers=(32, )):
        super().__init__('critic', 'c')
        self.add_parameter('layers', 'l', type=list, default=layers)

    def estimate_value(self, obs):
        x = obs
        for l in self['layers']:
            x = tf.layers.Dense(l)(x)
        return tf.layers.Dense(1)(x)


class VPGTrainedBaseline(VPGTrainer):

    def __init__(self, model, env, baseline_model=MLPValueModel(), optimizer=tf.train.AdamOptimizer, **kwargs):
        super().__init__(model, env, **kwargs)
        self.add_child(baseline_model)
        self._baseline_optimizer = optimizer
        self._baseline_loss = None
        self._baseline_train_op = None

        self.add_parameter('baseline_learning_rate', 'blr', type=float, default=0.01)

        self._state.add_variable('baseline_loss', 0, log=True)

    def initialize(self):
        super().initialize()

    def build_baseline_graph(self):
        self._graph_inputs['return_b'] = tf.placeholder(tf.float32, shape=(None, ), name="return_ph")

        self._model_outputs['baseline_estimate_t'] = \
            self['critic'].estimate_value(self._graph_inputs['observation_bf'])

        self._baseline_loss = \
            tf.losses.mean_squared_error(tf.squeeze(self._graph_inputs['return_b']),
                                         self._model_outputs['baseline_estimate_t'])
        self._baseline_optimizer = self._baseline_optimizer(self['baseline_learning_rate'])
        self._baseline_train_op = self._baseline_optimizer.minimize(self._baseline_loss)

    def build_graph(self):
        super().build_graph()
        self.build_baseline_graph()

    def epoch_update(self):
        value_estimate_t = self._sess_run(self._model_outputs['baseline_estimate_t'],
                                          feed_dict={'observation'
                                                     '_bf': self._state['epoch_observations_tf']})

        advantage_t = self._state['epoch_returns_t'] - np.squeeze(value_estimate_t)

        self._state['loss'], _ = self._sess_run((self._loss, self._train_op), feed_dict={'observation_bf': self._state['epoch_observations_tf'],
                                                  'action_t': self._state['epoch_actions_t'],
                                                  'advantage_b': advantage_t})

        self._state['baseline_loss'], _ = self._sess_run((self._baseline_loss, self._baseline_train_op), feed_dict={'observation_bf': self._state['epoch_observations_tf'],
                                                              'return_b': self._state['epoch_returns_t']})

