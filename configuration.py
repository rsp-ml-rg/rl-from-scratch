import argparse
import re
import random


class ConfigurationParameter(object):

    def __init__(self, long_name, short_name=None, type=None, default=None, parent=None, metaparameter=True,
                 description=''):
        '''
        Used as a parameter of a ConfigurableObject. Is added as an argument to ArgumentParser within the Configuration
        object.
        :param long_name: str
        :param short_name: str
        :param type: type
        :param default:
        :param parent: ConfigurableObject
        :param metaparameter: bool: Should this be included e.g. in log names? Should be True for most parameters;
                False typically for things like log dirs or other logging settings.
        '''
        self._name = long_name
        self._short_name = short_name if short_name is not None else long_name
        self._type = type
        self._default = default
        self.metaparameter = metaparameter
        self.description = description

        self._value = None

        self._parent = parent

    @property
    def full_path(self):
        '''
        Full name of this parameter - long names of this and its parents split by dots
        :return: str
        '''
        if self._parent is not None:
            return f'{self._parent.full_path}.{self._name}'
        else:
            return self._name

    @property
    def short_path(self):
        if self._parent is not None:
            return f'{self._parent.short_path}.{self._short_name}'
        else:
            return self._short_name

    def set_to_default(self):
        self._value = self._default

    @property
    def value(self):
        return self._value

    def set_value(self, value):
        self._value = value

    @property
    def name(self):
        return self._name

    @property
    def type(self):
        return self._type

    @property
    def default(self):
        return self._default

    def is_default(self):
        return self._value == self._default


class ConfigurationBase(object):

    def __init__(self):
        self._children = {}
        self._parameters = {}

    def check_key_not_present(self, k):
        if k in self._children or k in self._parameters:
            raise ValueError("Parameters and children of one ConfigurableObject must have unique names.")

    def add_child(self, child):
        self.check_key_not_present(child.name)
        self._children[child.name] = child
        child.set_parent(self, dont_add_to_parent=True)

    def is_parent_of(self, child):
        return child in self._children.values()

    @property
    def param_dict(self):
        d = {}
        d['class'] = self.__class__.__name__

        for param in self._parameters.values():
            d[param.name] = param.value

        for child in self._children.values():
            d[child.name] = child.param_dict

        return d

    def __getitem__(self, k):
        if k in self._parameters:
            return self._parameters[k].value
        else:
            return self._children[k]


class Configuration(ConfigurationBase):

    full_path = ''
    short_path = ''

    def __init__(self):
        '''
        Central point for handling configuration parameters. Should be the root of the configuration tree.
        In general (unless running multiple experiments e.g. as a metasearch) should not be instantiated outside of this
        file.
        '''
        super().__init__()

        self._all_parameters = []

        self._parser = argparse.ArgumentParser()

        self._children_dict = {}

        self._config_dict = {}

    def add_parameter(self, parameter):
        self._parser.add_argument('--' + parameter.full_path, '-' + parameter.short_path, type=parameter.type,
                                  default=parameter.default, help=parameter.description)
        self._all_parameters.append(parameter)

    def parse_arguments(self):
        args = self._parser.parse_args()

        # Set values from config_dict
        self.add_config(set_values=True)

        # For params no set in config_dict, get them from arg parser
        for param in self._all_parameters:
            if param.full_path not in self._config_dict:
                if param.full_path in args:
                    param.set_value(args.__getattribute__(param.full_path))
                else:
                    param.set_to_default()
                    print(f"Param {param.full_path} not found in the parser or config_dict. Setting to default of {param.value}")

    def set_to_defaults(self):
        for param in self._all_parameters:
            param.set_to_default()

    @property
    def param_dict(self):

        d = {}

        # for param in self._top_level_parameters:
        #     d[param.name] = param.value

        for name, child in self._children.items():
            d[name] = child.param_dict

        return d

    @property
    def path2param(self):
        return {param.full_path: param for param in self._all_parameters}

    def param_id_string(self):
        param_strings = [
            param.short_path+'='+str(param.value) for param in self._all_parameters
            if (param.metaparameter and not param.is_default())
        ]
        param_strings.sort()
        id_str = '_'.join(param_strings)
        id_str = re.sub(r'\s', '', id_str)

        return id_str

    def add_config(self, config_dict={}, set_values=False):
        self._config_dict.update(config_dict)

        if set_values:
            params = self.path2param

            for k, v in self._config_dict.items():
                params[k].set_value(v)

    def randomly_sample(self, sampling_sets):
        config = {}

        for param_path, sampling_domain in sampling_sets.items():
            config[param_path] = random.choice(sampling_domain)

        self.add_config(config, set_values=True)

        return config

    def __getitem__(self, k):
        return self._children[k]

    def reset(self):
        return self.__init__()


configuration = Configuration()


class ConfigurableObject(ConfigurationBase):

    def __init__(self, long_name, short_name=None, parent=configuration):

        super().__init__()
        self.name = long_name
        self.short_name = short_name if short_name is not None else long_name
        self.parent = parent
        parent.add_child(self)

    def add_parameter(self, long_name, short_name=None, type=None, default=None, description=''):
        param = ConfigurationParameter(long_name, short_name, type=type, default=default, parent=self,
                                       description=description)
        self.check_key_not_present(param.name)
        self._parameters[param.name] = param
        configuration.add_parameter(param)

    def set_parent(self, parent, dont_add_to_parent=False):
        self.parent = parent
        if not dont_add_to_parent:
            parent.add_child(self)

    @property
    def full_path(self):
        if self.parent is not configuration:
            return f'{self.parent.full_path}.{self.name}'
        else:
            return self.name

    @property
    def short_path(self):
        if self.parent is not configuration:
            return f'{self.parent.short_path}.{self.short_name}'
        else:
            return self.short_name
