import subprocess


def flatten_shape(shape):
    length = 1
    for s in shape:
        length *= s
    return length


def get_revision_hash():
        try:
            git_output = str(subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']), 'utf-8').strip()
            return git_output
        except subprocess.CalledProcessError:
            return None